import React from 'react'
import { render } from '../testUtils'
import Home from "../../pages";

describe('Home page', () => {
    it('sample', () => {
        const { getByText } = render(<Home />, {})
        expect(getByText('Find in-depth information about Next.js features and API.')).toBeInTheDocument()
    })
})
