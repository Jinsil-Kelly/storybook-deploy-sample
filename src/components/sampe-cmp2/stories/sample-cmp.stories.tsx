import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';
import {SampleCmp} from "../sample-cmp";

// eslint-disable-next-line import/no-default-export
export default {
  title: 'AppSection2',
  component: SampleCmp,
} as ComponentMeta<typeof SampleCmp>;

const Template: ComponentStory<typeof SampleCmp> = args => (
  <SampleCmp />
);

export const Default = Template.bind({});
